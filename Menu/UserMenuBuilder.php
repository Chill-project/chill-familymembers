<?php
/*
 */
namespace Chill\AMLI\FamilyMembersBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Chill\AMLI\FamilyMembersBundle\Security\Voter\FamilyMemberVoter;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class UserMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     *
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;
    
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker, 
        TranslatorInterface $translator
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->translator = $translator;
    }

    
    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $parameters['person'];
        
        if ($this->authorizationChecker->isGranted(FamilyMemberVoter::SHOW, $person)) {
            $menu->addChild(
                $this->translator->trans('Family memberships'), [
                    'route' => 'chill_family_members_family_members_index',
                    'routeParameters' => [ 'id' => $person->getId() ],
                ])
                ->setExtra('order', 450)
                ;
        }
    }

    public static function getMenuIds(): array
    {
        return [ 'person' ];
    }
}
