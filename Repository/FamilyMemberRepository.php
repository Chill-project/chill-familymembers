<?php

namespace Chill\AMLI\FamilyMembersBundle\Repository;

use Chill\PersonBundle\Entity\Person;

/**
 * FamilyMemberRepository
 *
 */
class FamilyMemberRepository extends \Doctrine\ORM\EntityRepository
{
    public function findActiveByPerson(Person $person)
    {
        return $this->findBy([ 'person' => $person ]);
    }
}
