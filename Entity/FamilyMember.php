<?php

namespace Chill\AMLI\FamilyMembersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FamilyMember
 *
 * @ORM\Table(name="chill_family.family_member")
 * @ORM\Entity(repositoryClass="Chill\AMLI\FamilyMembersBundle\Repository\FamilyMemberRepository")
 */
class FamilyMember extends AbstractFamilyMember
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
