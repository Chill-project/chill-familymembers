<?php
/*
 * 
 */
namespace Chill\AMLI\FamilyMembersBundle\Templating;

use Twig\Extension\AbstractExtension;
use Chill\AMLI\FamilyMembersBundle\Config\ConfigRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Twig\TwigFunction;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class Twig extends AbstractExtension
{
    /**
     *
     * @var ConfigRepository
     */
    protected $configRepository;
    
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    public function __construct(
        ConfigRepository $configRepository, 
        TranslatableStringHelper $translatableStringHelper
    ) {
        $this->configRepository = $configRepository;
        $this->translatableStringHelper = $translatableStringHelper;
    }

    
    public function getFilters()
    {
        return [
            new \Twig_Filter('chill_family_member_link_display', [ $this, 'displayLink' ], [ 'is_safe' => [ 'html' ]]),
            new \Twig_Filter('chill_family_member_professional_situation_display', [ $this, 'displayProfessionalSituation' ], [ 'is_safe' => [ 'html' ]]),
            new \Twig_Filter('chill_family_member_familial_situation_display', [ $this, 'displayFamilialSituation' ], [ 'is_safe' => [ 'html' ]]),

        ];
    }
    
    public function getFunctions()
    {
        return [
            new TwigFunction('chill_family_members_has_professionnal_situation', [ $this, 'hasProfessionnalSituation' ]),
            new TwigFunction('chill_family_members_has_familial_situation', [ $this, 'hasFamilialSituation' ]),

        ];
    }
    
    public function displayLink($link)
    {
        if (NULL === $link) {
            return null;
        }
        
        return $this->translatableStringHelper->localize(
            $this->configRepository->getLinksLabels()[$link]
            );
    }
    
    public function displayProfessionalSituation($situation)
    {
        if (NULL === $situation) {
            return null;
        }
        
        return $this->translatableStringHelper->localize(
            $this->configRepository->getProfessionalSituationsLabels()[$situation]
            );
    }
    
    public function hasProfessionnalSituation()
    {
        return $this->configRepository->hasProfessionalSituation();
    }
    
    public function displayFamilialSituation($situation)
    {
        if (NULL === $situation) {
            return null;
        }
        
        return $this->translatableStringHelper->localize(
            $this->configRepository->getFamilialSituationsLabels()[$situation]
            );
    }
    
    public function hasFamilialSituation()
    {
        return $this->configRepository->hasFamilialSituation();
    }
}
