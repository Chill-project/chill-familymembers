<?php

namespace Chill\AMLI\FamilyMembersBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('chill_amli_family_members');

        $rootNode
            ->children()
                ->arrayNode('links')->isRequired()->requiresAtLeastOneElement()
                    ->arrayPrototype()
                        ->children()
                        ->scalarNode('key')->isRequired()->cannotBeEmpty()
                            ->info('the key stored in database')
                                ->example('grandson')
                            ->end()
                            ->arrayNode('labels')->isRequired()->requiresAtLeastOneElement()
                                ->arrayPrototype()
                                    ->children()
                                        ->scalarNode('lang')->isRequired()->cannotBeEmpty()
                                        ->example('fr')
                                    ->end()
                                    ->scalarNode('label')->isRequired()->cannotBeEmpty()
                                         ->example('Petit-fils')
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('professionnal_situations')->isRequired()
                    ->info("the list of professional situations. If empty, the field will not be shown")
                    ->arrayPrototype()
                        ->children()
->scalarNode('key')->isRequired()->cannotBeEmpty()
                            ->info('the key stored in database')
                                ->example('student')
                            ->end()
                            ->arrayNode('labels')->isRequired()->requiresAtLeastOneElement()
                                ->arrayPrototype()
                                    ->children()
                                        ->scalarNode('lang')->isRequired()->cannotBeEmpty()
                                        ->example('fr')
                                    ->end()
                                    ->scalarNode('label')->isRequired()->cannotBeEmpty()
                                         ->example('Étudiant')
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->end()
                ->arrayNode('familial_situations')->isRequired()
                    ->info("the list of familial situations. If empty, the field will not be shown")
                    ->arrayPrototype()
                        ->children()
->scalarNode('key')->isRequired()->cannotBeEmpty()
                            ->info('the key stored in database')
                                ->example('half_time_keeping')
                            ->end()
                            ->arrayNode('labels')->isRequired()->requiresAtLeastOneElement()
                                ->arrayPrototype()
                                    ->children()
                                        ->scalarNode('lang')->isRequired()->cannotBeEmpty()
                                        ->example('fr')
                                    ->end()
                                    ->scalarNode('label')->isRequired()->cannotBeEmpty()
                                         ->example('En garde alternée')
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ;

        return $treeBuilder;
    }
}
