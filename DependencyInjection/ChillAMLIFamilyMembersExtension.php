<?php

namespace Chill\AMLI\FamilyMembersBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Chill\AMLI\FamilyMembersBundle\Security\Voter\FamilyMemberVoter;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ChillAMLIFamilyMembersExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        
        $this->storeConfig($container, $config);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services/config.yml');
        $loader->load('services/form.yml');
        $loader->load('services/controller.yml');
        $loader->load('services/security.yml');
        $loader->load('services/menu.yml');
        $loader->load('services/templating.yml');
    }
    
    private function storeConfig(ContainerBuilder $container, array $config)
    {
        $container->setParameter('chill_family_members.links', $config['links']);
        $container->setParameter('chill_family_members.professionnal_situations', 
            $config['professionnal_situations']);
                $container->setParameter('chill_family_members.familial_situations', 
            $config['familial_situations']);
    }
    
    public function prepend(ContainerBuilder $container)
    {
        $this->prependAuthorization($container);
        $this->prependRoutes($container);
    }
    
    protected function prependAuthorization(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', array(
           'role_hierarchy' => array(
               FamilyMemberVoter::UPDATE => [FamilyMemberVoter::SHOW],
               FamilyMemberVoter::CREATE => [FamilyMemberVoter::SHOW]
           )
        ));
    }
    
    /* (non-PHPdoc)
     * @see \Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface::prepend()
     */
    public function prependRoutes(ContainerBuilder $container) 
    {
        //add routes for custom bundle
         $container->prependExtensionConfig('chill_main', array(
           'routing' => array(
              'resources' => array(
                 '@ChillAMLIFamilyMembersBundle/Resources/config/routing.yml'
              )
           )
        ));
    }

}
