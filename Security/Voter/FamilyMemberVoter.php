<?php
/*
 */
namespace Chill\AMLI\FamilyMembersBundle\Security\Voter;

use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Chill\AMLI\FamilyMembersBundle\Entity\FamilyMember;
use Chill\PersonBundle\Entity\Person;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Entity\User;
use Symfony\Component\Security\Core\Role\Role;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class FamilyMemberVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{
    const CREATE = 'CHILL_FAMILY_MEMBERS_FAMILY_MEMBERS_CREATE';
    const DELETE = 'CHILL_FAMILY_MEMBERS_FAMILY_MEMBERS_DELETE';
    const UPDATE = 'CHILL_FAMILY_MEMBERS_FAMILY_MEMBERS_UPDATE';
    const SHOW   = 'CHILL_FAMILY_MEMBERS_FAMILY_MEMBERS_SHOW';
    
    const ROLES = [
        self::CREATE,
        self::DELETE,
        self::SHOW,
        self::UPDATE
    ];
    
    /**
     *
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;
    
    public function __construct(AuthorizationHelper $authorizationHelper)
    {
        $this->authorizationHelper = $authorizationHelper;
    }

    
    protected function supports($attribute, $subject)
    {
        return (\in_array($attribute, self::ROLES) && $subject instanceof FamilyMember)
            or
                ($subject instanceof Person && \in_array($attribute, [ self::SHOW, self::CREATE ]));
    }
    
    protected function voteOnAttribute($attribute, $subject, \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token)
    {
        $user = $token->getUser();
        
        if (FALSE === $user instanceof User) {
            return false;
        }
        
        return $this->authorizationHelper
            ->userHasAccess($user, $subject, new Role($attribute));
    }
    
    public function getRoles()
    {
        return self::ROLES;
    } 
    
    public function getRolesWithHierarchy(): array
    {
        return [ 'Family Members' => self::ROLES ];
    } 
    
    public function getRolesWithoutScope()
    {
        return self::ROLES;
    }

}
