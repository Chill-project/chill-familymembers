<?php

namespace Chill\AMLI\FamilyMembersBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Chill\PersonBundle\Entity\Person;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Chill\AMLI\FamilyMembersBundle\Entity\FamilyMember;
use Chill\AMLI\FamilyMembersBundle\Security\Voter\FamilyMemberVoter;
use Chill\AMLI\FamilyMembersBundle\Form\FamilyMemberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Translation\TranslatorInterface;
use Psr\Log\LoggerInterface;

class FamilyMemberController extends Controller
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    /**
     *
     * @var LoggerInterface
     */
    protected $chillMainLogger;
    
    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        LoggerInterface $chillMainLogger
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->chillMainLogger = $chillMainLogger;
    }

    
    /**
     * @Route(
     *  "{_locale}/family-members/family-members/by-person/{id}",
     *  name="chill_family_members_family_members_index"
     * )
     */
    public function indexAction(Person $person)
    {
        $this->denyAccessUnlessGranted(FamilyMemberVoter::SHOW, $person);
        
        $familyMembers = $this->em
            ->getRepository(FamilyMember::class)
            ->findByPerson($person);
        
        return $this->render('ChillAMLIFamilyMembersBundle:FamilyMember:index.html.twig', array(
            'person' => $person,
            'familyMembers' => $familyMembers
        ));
    }

    /**
     * @Route(
     *  "{_locale}/family-members/family-members/by-person/{id}/new",
     *  name="chill_family_members_family_members_new"
     * )
     */
    public function newAction(Person $person, Request $request)
    {
        $familyMember = (new FamilyMember())
            ->setPerson($person)
            ;
        
        $this->denyAccessUnlessGranted(FamilyMemberVoter::CREATE, $familyMember);
        
        $form = $this->createForm(FamilyMemberType::class, $familyMember);
        $form->add('submit', SubmitType::class);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() and $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($familyMember);
            $em->flush();
            
            $this->addFlash('success', $this->translator->trans('Family member created'));
            
            return $this->redirectToRoute('chill_family_members_family_members_index', [
                'id' => $person->getId()
            ]);
        }
        
        return $this->render('ChillAMLIFamilyMembersBundle:FamilyMember:new.html.twig', array(
            'form' => $form->createView(),
            'person' => $person
        ));
    }

    /**
     * @Route(
     *  "{_locale}/family-members/family-members/{id}/edit",
     *  name="chill_family_members_family_members_edit"
     * )
     */
    public function editAction(FamilyMember $familyMember, Request $request)
    {
        $this->denyAccessUnlessGranted(FamilyMemberVoter::UPDATE, $familyMember);
        
        $form = $this->createForm(FamilyMemberType::class, $familyMember);
        $form->add('submit', SubmitType::class);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() and $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            
            $this->addFlash('success', $this->translator->trans('Family member updated'));
            
            return $this->redirectToRoute('chill_family_members_family_members_index', [
                'id' => $familyMember->getPerson()->getId()
            ]);
        }
        
        return $this->render('ChillAMLIFamilyMembersBundle:FamilyMember:edit.html.twig', array(
            'familyMember' => $familyMember,
            'form' => $form->createView(),
            'person' => $familyMember->getPerson()
        ));
    }
    
    /**
     * 
     * @Route(
     *  "{_locale}/family-members/family-members/{id}/delete",
     *  name="chill_family_members_family_members_delete"
     * )
     * 
     * @param FamilyMember $familyMember
     * @param Request $request
     * @return \Symfony\Component\BrowserKit\Response
     */
    public function deleteAction(FamilyMember $familyMember, Request $request)
    {
        $this->denyAccessUnlessGranted(FamilyMemberVoter::DELETE, $familyMember, 'You are not '
             . 'allowed to delete this family membership');

        $form = $this->createDeleteForm($id);

        if ($request->getMethod() === Request::METHOD_DELETE) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->chillMainLogger->notice("A family member has been removed", array(
                   'by_user' => $this->getUser()->getUsername(),
                   'family_member_id' => $familyMember->getId(),
                   'name' => $familyMember->getFirstname()." ".$familyMember->getLastname(),
                   'link' => $familyMember->getLink()
                ));

                $em = $this->getDoctrine()->getManager();
                $em->remove($familyMember);
                $em->flush();

                $this->addFlash('success', $this->translator
                        ->trans("The family member has been successfully removed."));

                return $this->redirectToRoute('chill_family_members_family_members_index', array(
                     'id' => $familyMember->getPerson()->getId()
                  ));
            }
        }


        return $this->render('ChillAMLIFamilyMembersBundle:FamilyMember:confirm_delete.html.twig', array(
           'familyMember' => $familyMember,
           'delete_form' => $form->createView()
        ));
    }
    
    /**
     * @Route(
     *  "{_locale}/family-members/family-members/{id}/view",
     *  name="chill_family_members_family_members_view"
     * )
     */
    public function viewAction(FamilyMember $familyMember)
    {
        $this->denyAccessUnlessGranted(FamilyMemberVoter::SHOW, $familyMember);
        
        return $this->render('ChillAMLIFamilyMembersBundle:FamilyMember:view.html.twig', array(
            'familyMember' => $familyMember
        ));
    }
    
    /**
     * Creates a form to delete a help request entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setMethod(Request::METHOD_DELETE)
            ->add('submit', SubmitType::class, array('label' => 'Delete'))
            ->getForm()
        ;
    }

}
