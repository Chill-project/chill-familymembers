<?php

namespace Chill\AMLI\FamilyMembersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\PersonBundle\Form\Type\GenderType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Chill\MainBundle\Form\Type\ChillDateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Chill\PersonBundle\Form\Type\Select2MaritalStatusType;
use Chill\AMLI\FamilyMembersBundle\Config\ConfigRepository;
use Chill\AMLI\FamilyMembersBundle\Entity\FamilyMember;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\AMLI\AssignmentBundle\Entity\AbstractDiagnosticNIAssignment;
use Chill\AMLI\FamilyMembersBundle\Entity\AbstractFamilyMember;

class FamilyMemberType extends AbstractType
{
    /**
     *
     * @var ConfigRepository
     */
    private $configRepository;
    
    /**
     *
     * @var TranslatableStringHelper
     */
    private $translatableStringHelper;
    
    public function __construct(
        ConfigRepository $configRepository, 
        TranslatableStringHelper $translatableStringHelper
    ) {
        $this->configRepository = $configRepository;
        $this->translatableStringHelper = $translatableStringHelper;
    }

    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      
        $builder
            ->add('lastname', TextType::class, [
                'label' => 'Last name' 
            ])
            ->add('firstname', TextType::class, [
                'label' => 'First name'
            ])
            ->add('gender', GenderType::class)
            ->add('birthdate', ChillDateType::class, [
                'required' => false
            ])
            ->add('link', ChoiceType::class, [
                'choices' => $this->buildChoices($this->configRepository->getLinksLabels()),
                'placeholder' => 'Choose a link',
                'label' => 'Relationship'
            ])
            ->add('maritalStatus', Select2MaritalStatusType::class, [
                'required' => false
            ])
            ;
        
        if ($this->configRepository->hasProfessionalSituation()) {
            $builder
                    ->add('professionnalSituation', ChoiceType::class, [
                        'required' => false,
                        'choices' => $this->buildChoices(
                            $this->configRepository->getProfessionalSituationsLabels()
                            )
                    ]);
        }
        
        if ($this->configRepository->hasProfessionalSituation()) {
            $builder
                    ->add('familialSituation', ChoiceType::class, [
                        'required' => false,
                        'choices' => $this->buildChoices(
                            $this->configRepository->getFamilialSituationsLabels()
                            )
                    ]);
        }
        
        if ($options['show_start_date']) {
            $builder
                ->add('startDate', ChillDateType::class, [
                    'label' => 'Arrival date in the family'
                ]);
        }
        
        if ($options['show_end_date']) {
            $builder
                ->add('endDate', ChillDateType::class, [
                    'required' => false,
                    'label' => 'Departure date of the family'
                ]);
        }

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => FamilyMember::class,
            'show_start_date' => true,
            'show_end_date' => true,
        ));
        
        $resolver
            ->setAllowedTypes('show_start_date', 'boolean')
            ->setAllowedTypes('show_end_date', 'boolean')
            ;
    }
    
    private function buildChoices($els)
    {
        $links = $this->configRepository
            ->getLinksLabels();
        $choices = [];
        
        // rewrite labels to filter in language
        foreach ($els as $key => $labels) {
            $choices[$this->translatableStringHelper->localize($labels)] = $key;
        }
        
        return $choices;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'chill_amli_familymembersbundle_familymember';
    }


}
