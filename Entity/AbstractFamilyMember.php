<?php
/*
 * 
 */
namespace Chill\AMLI\FamilyMembersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Entity\MaritalStatus;
use Chill\MainBundle\Entity\HasCenterInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * 
 * @ORM\MappedSuperclass()
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
abstract class AbstractFamilyMember implements HasCenterInterface
{
    const FAMILIAL_SITUATION = [
         'a_charge'=> 'A charge',
         'garde_alternee'=> 'Garde alternée',
         'droit_de_visite'=> 'Droit de visite et d\'hébergement',
         'non_a_charge' => 'Non à charge',
       ];

/**
     *
     * @var Person
     * @ORM\ManyToOne(
     *  targetEntity="\Chill\PersonBundle\Entity\Person"
     * )
     */
    private $person;
    
    /**
     *
     * @var MaritalStatus
     * @ORM\ManyToOne(
     *  targetEntity="\Chill\PersonBundle\Entity\MaritalStatus"
     * )
     */
    private $maritalStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    private $lastname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255)
     */
    private $firstname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=20)
     */
    private $gender = '';

    /**
     * @var date_immutable|null
     *
     * @ORM\Column(name="birthdate", type="date_immutable", nullable=true)
     * @Assert\Date()
     */
    private $birthdate;

    /**
     * @var string
     *
     * @ORM\Column(name="professionnalSituation", type="text")
     */
    private $professionnalSituation = '';

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255)
     */
    private $link = '';
    
    /**
     *
     * @var string
     * @ORM\Column(name="familial_situation", type="string", length=200, nullable=true)
     */
    private $familialSituation;

    /**
     * @var \DateTimeImmutable
     *
     * @ORM\Column(name="startDate", type="datetime_immutable")
     * @Assert\NotNull()
     * @Assert\Date()
     */
    private $startDate;

    /**
     * @var \DateTimeImmutable|null
     *
     * @ORM\Column(name="endDate", type="datetime_immutable", nullable=true)
     * @Assert\Date()
     * @Assert\GreaterThan(
     *  propertyPath="startDate",
     *  message="The membership's end date should be after the start date"
     * )
     */
    private $endDate;
    
    public function __construct()
    {
        $this->setStartDate(new \DateTimeImmutable('now'));
    }
    
    /**
     * Set lastname.
     *
     * @param string $lastname
     *
     * @return FamilyMember
     */
    public function setLastname($lastname)
    {
        $this->lastname = (string) $lastname;

        return $this;
    }

    /**
     * Get lastname.
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname.
     *
     * @param string $firstname
     *
     * @return FamilyMember
     */
    public function setFirstname($firstname)
    {
        $this->firstname = (string) $firstname;

        return $this;
    }

    /**
     * Get firstname.
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set gender.
     *
     * @param string $gender
     *
     * @return FamilyMember
     */
    public function setGender($gender)
    {
        $this->gender = (string) $gender;

        return $this;
    }

    /**
     * Get gender.
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set birthdate.
     *
     * @param \DateTimeInterface|null $birthdate
     *
     * @return FamilyMember
     */
    public function setBirthdate(\DateTimeInterface $birthdate = null)
    {
        if ($birthdate instanceof \DateTime) {
            $this->birthdate = \DateTimeImmutable::createFromMutable($birthdate);
        } elseif ($birthdate === null || $birthdate instanceof \DateTimeImmutable) {
            $this->birthdate = $birthdate;
        }

        return $this;
    }

    /**
     * Get birthdate.
     *
     * @return date_immutable|null
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set professionnalSituation.
     *
     * @param string $professionnalSituation
     *
     * @return FamilyMember
     */
    public function setProfessionnalSituation($professionnalSituation)
    {
        $this->professionnalSituation = (string) $professionnalSituation;

        return $this;
    }

    /**
     * Get professionnalSituation.
     *
     * @return string
     */
    public function getProfessionnalSituation()
    {
        return $this->professionnalSituation;
    }

    /**
     * Set link.
     *
     * @param string $link
     *
     * @return FamilyMember
     */
    public function setLink($link)
    {
        $this->link = (string) $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }
    
    public function getFamilialSituation()
    {
        return $this->familialSituation;
    }

    public function setFamilialSituation($familialSituation)
    {
        $this->familialSituation = $familialSituation;
        
        return $this;
    }
    
    /**
     * Set startDate.
     *
     * @param \DateTimeImmutable $startDate
     *
     * @return FamilyMember
     */
    public function setStartDate(\DateTimeInterface $startDate)
    {
        if ($startDate instanceof \DateTime) {
            $this->startDate = \DateTimeImmutable::createFromMutable($startDate);
        } elseif (NULL === $startDate || $startDate instanceof \DateTimeImmutable) {
            $this->startDate = $startDate;
        }

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTimeImmutable
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTimeInterface|null $endDate
     *
     * @return FamilyMember
     */
    public function setEndDate(\DateTimeInterface $endDate = null)
    {
        if ($endDate instanceof \DateTime) {
            $this->endDate = \DateTimeImmutable::createFromMutable($endDate);
        } elseif ($endDate instanceof \DateTimeImmutable || $endDate === null) {
            $this->endDate = $endDate;
        }

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTimeImmutable|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }
    
    /**
     * 
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * 
     * @return MaritalStatus
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     * 
     * @param Person $person
     * @return $this
     */
    public function setPerson(Person $person)
    {
        $this->person = $person;
        
        return $this;
    }

    /**
     * 
     * @param MaritalStatus $maritalStatus
     * @return $this
     */
    public function setMaritalStatus(MaritalStatus $maritalStatus = null)
    {
        $this->maritalStatus = $maritalStatus;
        
        return $this;
    }

    public function getCenter(): \Chill\MainBundle\Entity\Center
    {
        return $this->getPerson()->getCenter();
    }
}
