<?php
/*
 */
namespace Chill\AMLI\FamilyMembersBundle\Config;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ConfigRepository
{
    /**
     *
     * @var array
     */
    protected $links;
    
    /**
     *
     * @var array
     */
    protected $professionalSituations;
    
    /**
     *
     * @var array
     */
    protected $familialSituations;
    
    public function __construct($links, $professionnalSituations, $familialSituations)
    {
        $this->links = $links;
        $this->professionalSituations = $professionnalSituations ?? [];
        $this->familialSituations = $familialSituations ?? [];
    }
    
    /**
     * 
     * @return array where keys are the link's keys and label the links label
     */
    public function getLinksLabels()
    {
        return $this->normalizeConfig($this->links);
    }
    
    public function getProfessionalSituationsLabels()
    {
        return $this->normalizeConfig($this->professionalSituations);
    }
    
    public function hasProfessionalSituation(): bool
    {
        return count($this->professionalSituations) > 0;
    }
    
    public function getFamilialSituationsLabels()
    {
        return $this->normalizeConfig($this->familialSituations);
    }
    
    public function hasFamilialSituation(): bool
    {
        return count($this->familialSituations) > 0;
    }
    
    private function normalizeConfig($config)
    {
        $els = array();
        
        foreach ($config as $definition) {
            $els[$definition['key']] = $this->normalizeLabel($definition['labels']);
        }
        
        return $els;
    }
    
    private function normalizeLabel($labels) 
    {
        $normalizedLabels = array();
        
        foreach ($labels as $labelDefinition) {
            $normalizedLabels[$labelDefinition['lang']] = $labelDefinition['label'];
        }
        
        return $normalizedLabels;
    }

}
