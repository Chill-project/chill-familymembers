<?php
/*
 */
namespace Chill\AMLI\FamilyMembersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Chill\MainBundle\Form\Type\ChillCollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class FamilyMembersType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'entry_type' => FamilyMemberType::class,
            'entry_options' => [
                'show_start_date' => true,
                'show_end_date' => true
            ],
            'allow_add' => true,
            'allow_delete' => true,
            'button_add_label' => 'Ajouter un membre',
            'button_remove_label' => 'Enlever ce membre'
        ]);
    }
    
    public function getParent()
    {
        return ChillCollectionType::class;
    }
}
