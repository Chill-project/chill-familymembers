<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * create schema for chill_family
 */
final class Version20180522142023 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA chill_family');
        $this->addSql('CREATE SEQUENCE chill_family.family_member_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_family.family_member (id INT NOT NULL, person_id INT DEFAULT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, gender VARCHAR(20) NOT NULL, birthdate DATE DEFAULT NULL, professionnalSituation TEXT NOT NULL, link VARCHAR(255) NOT NULL, startDate TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, endDate TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, maritalStatus_id VARCHAR(7) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A61F4A49217BBB47 ON chill_family.family_member (person_id)');
        $this->addSql('CREATE INDEX IDX_A61F4A49D7D03CE3 ON chill_family.family_member (maritalStatus_id)');
        $this->addSql('COMMENT ON COLUMN chill_family.family_member.birthdate IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_family.family_member.startDate IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN chill_family.family_member.endDate IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE chill_family.family_member ADD CONSTRAINT FK_A61F4A49217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_family.family_member ADD CONSTRAINT FK_A61F4A49D7D03CE3 FOREIGN KEY (maritalStatus_id) REFERENCES chill_person_marital_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SCHEMA chill_family CASCADE');
        
    }
}
